public class Cricket
{
	//static refers to class specific information
	static String name = "Thaniga";
	static int age = 17;
	//non-static refers to object(instance) specific information
	String name2 = "Kalanidhi";
    int age2 = 15;
	//String name3 = "Siva";
    //int age3 = 19;
	//String name = "hari";
public static void main(String[] args)
{
	//local variable(method, block, constructor)
	int no = 45;
	//String name = "hari";
	System.out.println(no);
	System.out.println(Cricket.name);
	System.out.println(Cricket.age);
	
	Cricket player1 = new Cricket();
	System.out.println(player1.name2);
    System.out.println(player1.age2);
}

}
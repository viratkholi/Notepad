public class Kalanidhi
{
 //static refers to class specific information
 static String name = "Virat";
 static int age = 35;
 //Non-static variable or instance variable
 String name2 = "Ganguly";
 int age2 = 68;
 boolean stud;
 double weight = 69.77;
 
 //byte short int long -> 0
 //double float -> 0.0
 //boolean - false
 //string - null
 //char - ''

public static void main(String[] args)
{ //local variable -> method , block , constructor
 int no = 45;
 //Kalanidhi.name="Dhoni";
 //Kalanidhi.name="Sachin";
 System.out.println(no);
 

 Kalanidhi person1 = new Kalanidhi();
 person1.name2="Arivoli";
 person1.age2=20;
	//person1.name2="Sakthi";
	
 
 Kalanidhi person2 = new Kalanidhi();
 //person2.name2="Abi";
 //person2.age2=28;
 
 Kalanidhi person3 = new Kalanidhi();
 
 System.out.println(Kalanidhi.name); 
 System.out.println(name);
 System.out.println(person1.name); 
 System.out.println(person3.name);
 System.out.println(person2.name);
	System.out.println(Kalanidhi.age);
 Kalanidhi.age = 46;
	Kalanidhi.age = 47;
 System.out.println(Kalanidhi.age);
 System.out.println(person1.name2);//Arivoli
 System.out.println(person1.age2);
 System.out.println(person2.name2);//Abi
 System.out.println(person2.age2);
 
 System.out.println(person3.name2);//Ganguly
 System.out.println(person3.age2);
 System.out.println(person3.stud);
 System.out.println(person3.weight);
 
 //System.out.println(age3);
 
 System.out.println(person1.name);
 System.out.println(person2.name);
 
 
}
 
 
}
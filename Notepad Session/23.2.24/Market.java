public class Market
{
	static String marketName;
	static int doorNo=20;
	String name;
	int price;
public static void main(String[] args)
{
	// do not compile
	//int = no;
	//System.out.println(no);
	Market product1 = new Market();
	product1.name = "Biscuit";
	product1.price = 100;
	Market product2 = new Market();
	product2.name = "Choclate";
	product2.price = 200;
	Market product3 = new Market();
	product3.name = "Juice";
	product3.price = 300;
	Market product4 = new Market();
	
	System.out.println(product1.price);
	System.out.println(product2.name);
	System.out.println(product3.price);
	System.out.println(product4.price);
	System.out.println(Market.marketName);
	
	product4.buy();   //Method calling statement
	int balance = product4.buy();
    System.out.println(balance);
}
	//Method Definition
	
   /* public void buy() //void - No Return
{
	System.out.println("Buy Method");
}*/
	
	public int buy()  //Method signature
	
	{ //Method body
		
		System.out.println("Buy Method");
	    return 10;
		
	}
	
}
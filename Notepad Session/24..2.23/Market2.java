/*public class Market2
{
	
	public static void main(String[] args)
	{
		
      Market2 tag = new Market2();
	  tag.add();
		
	}
public void add()
{
	int no1 = 10;
	int no2 = 20;
	int no3 = no1+no2;
	System.out.println(no3);
}
}*/


//return
/*public class Market2
{
	
	public static void main(String[] args)
	{
		
      Market2 tag = new Market2();
	  int res = tag.add();
		System.out.println("main => "+res);
		
	}
public int add()
{
	int no1 = 10;
	int no2 = 20;
	int no3 = no1+no2;
	System.out.println("add => "+no3);
	return no3;
}
}*/
//easy way to write

public class Market2
{
	
	public static void main(String[] args)
	{
		
      Market2 tag = new Market2();
		
	  int res = tag.add();
		System.out.println("main => "+tag.add());
		tag.divide(10); //paramater
		tag.divide(100,5); //diff no of arg
		tag.divide('c');
		tag.divide("hello"); //diff type of arg
		
	}
public void divide(String str) 
	{
		System.out.println(str);
	}
	
public void divide(int no) //argument
	{
		System.out.println(no/2);
	}
public void divide(int no, int no2)
   {
	    System.out.println(no/no2);
   }
	public void divide(char v)
   {
	    System.out.println(v);
   }
	
public int add()
{
	int no1 = 10;
	int no2 = 20;
	int no3 = no1+no2;
	System.out.println("add => "+no3);
	return 10+20;
}
}
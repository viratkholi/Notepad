package Kanchipuram;

public class Conditional
{
	public static void main(String[] args)
	{
		
		int person1 = 20;
		int person2 = 22;
		
		if(person1 > person2)
		{
			System.out.println("person1");
		}
		else if(person2 > person1)
		{
			System.out.println("person2");
		}
		else
		{
			System.out.println("person1 and person2");
		}
		
		
	}
	
}